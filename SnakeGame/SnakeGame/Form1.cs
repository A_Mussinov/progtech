﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeGame
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Body body = new Body();
        Food food = new Food();
        Wall wall = new Wall(5);
        int direction = 0;
        Brush bg = new SolidBrush(Color.Black);
        Brush sn = new SolidBrush(Color.Green);
        Brush f = new SolidBrush(Color.Red);
        Brush w = new SolidBrush(Color.Blue);

        int score = 0;

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.FillRectangle(bg, 0, 0, 500, 500);
            g.FillEllipse(sn, body.body[0].X - 10, body.body[0].Y - 10, 20, 20);
            g.FillEllipse(f, food.location.X - 5, food.location.Y - 5, 10, 10);
            for (int i = 0; i < 5; i++)
            {
                g.FillRectangle(w, wall.obst[i].X - 5, wall.obst[i].Y - 50, 10, 100);
            }            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            for (int i = body.body.Count - 1; i >= 0; i--)
            {
                g.FillEllipse(bg, body.body[i].X - 10, body.body[i].Y - 10, 20, 20);
            }

            int dx = 0;
            int dy = 0;
            if (direction == 0)
            {
                dx = 5;
                dy = 0;
            }
            if (direction == 1)
            {
                dx = 0;
                dy = 5;
            }
            if (direction == 2)
            {
                dx = -5;
                dy = 0;
            }
            if (direction == 3)
            {
                dx = 0;
                dy = -5;
            }

            body.Move(dx, dy, food.location);
            if (body.Move(dx, dy, food.location))
            {
                food = new Food();
                label2.Text = score++.ToString();
            }
            for (int i = body.body.Count - 1; i >= 0; i--)
            {
                g.FillEllipse(sn, body.body[i].X - 10, body.body[i].Y - 10, 20, 20);
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right && direction != 2)
            {
                direction = 0;
            }
            if (e.KeyCode == Keys.Down && direction != 3)
            {
                direction = 1;
            }
            if (e.KeyCode == Keys.Left && direction != 0)
            {
                direction = 2;
            }
            if (e.KeyCode == Keys.Up && direction != 1)
            {
                direction = 3;
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
