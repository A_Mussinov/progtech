﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SnakeGame
{
    class Food
    {
        public Point location = new Point();
        public Food()
        {
            Random rnd = new Random();

            location.X = rnd.Next() % 500;
            location.Y = rnd.Next() % 500;
        }
    }
}
