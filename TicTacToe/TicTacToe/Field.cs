﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TicTacToe
{
    class Field
    {
        public int[,] f;
        public Field()
        {
            f = new int[3,3];
        }

        public int Check()
        {
            int res = 0;

            for (int i = 0; i < 3; i++)
            {
                if ((f[i, 0] == f[i, 1] && f[i, 1] == f[i, 2]) && f[i, 0] != 0)
                {
                    res = f[i, 0];
                    f[i, 0] = 3;
                    f[i, 1] = 3;
                    f[i, 2] = 3;
                }
            }
            if (res > 0) return res;
            for (int i = 0; i < 3; i++)
            {
                if ((f[0, i] == f[1, i] && f[1, i] == f[2, i]) && f[0, i] != 0)
                {
                    res = f[0, i];
                    f[0, i] = 3;
                    f[1, i] = 3;
                    f[2, i] = 3;
                }
            }
            if (res > 0) return res;
            if ((f[0, 0] == f[1, 1] && f[1, 1] == f[2, 2]) && f[1, 1] != 0)
            {
                res = f[1, 1];
                f[0, 0] = 3;
                f[1, 1] = 3;
                f[2, 2] = 3;
            }
            if (res > 0) return res;
            if ((f[0, 2] == f[1, 1] && f[1, 1] == f[2, 0]) && f[1, 1] != 0)
            {
                res = f[1,1];
                f[0, 2] = 3;
                f[1, 1] = 3;
                f[2, 0] = 3;
            }
            if (res > 0) return res;
            int draw = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (f[i, j] != 0)
                        draw++;
                }
            }
            if (draw == 9) res = 4;
            
            return res;
        }
    }
}
