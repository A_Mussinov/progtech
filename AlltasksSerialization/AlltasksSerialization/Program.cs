﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace AlltasksSerialization
{
    [Serializable]
    public class Student
    {
        public string name { get; set; }
        public string surname { get; set; }
        public string gender { get; set; }
        public double gpa { get; set; }

        public Student() { }

        public Student(string _name, string _surname, string  _gender, double _gpa)
        {
            name = _name;
            surname = _surname;
            gender = _gender;
            gpa = _gpa;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student s = new Student("Aslan", "Mussinov", "male", 100500);
            XmlSerializer xml = new XmlSerializer(typeof(Student));
            using (FileStream fs = new FileStream(@"C:\Аслан\KBTU\2 семестр\Programming Technologies\student.xml", FileMode.OpenOrCreate))
            {
                xml.Serialize(fs, s);
            }
        }
    }
}
