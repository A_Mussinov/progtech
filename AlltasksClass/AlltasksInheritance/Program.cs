﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlltasksInheritance
{
    abstract class Vehicle
    {
        private double coord1, coord2, price, speed, year;
        public double Coord1
        {
            get { return coord1; }
            set { coord1 = value; }
        }
        public double Coord2
        {
            get { return coord2; }
            set { coord2 = value; }
        }

        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        public double Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public double Year
        {
            get { return year; }
            set { year = value; }
        }
    }
    class Plane : Vehicle
    {
        public double Height { get; set; }
        public double Passengers { get; set; }
    }

    class Car : Vehicle
    {

    }

    class Ship : Vehicle
    {
        public double Passengers { get; set; }
        public string Port { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Plane p = new Plane(); //{ Coord1 = ...";
            Car c = new Car(); //{
            Ship s = new Ship(); //{

        }
    }
}
