﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlltasksAbstract
{
    abstract class Figure
    {
        abstract public string Perimeter();
        abstract public string Area();
        public override string ToString()
        {
            return Perimeter() + Area();
        }
    }
    class Rectangle : Figure
    {
        public double length;
        public double width;
        public Rectangle(double _length, double _width)
        {
            length = _length;
            width = _width;
        }
        public override string Perimeter()
        {
            return "The perimeter of the rectangle is " + 2 * (length + width) + "\n";
        }
        public override string Area()
        {
            return "The area of the rectangle is " + length * width + "\n";
        }
    }
    class Circle : Figure
    {
        public double radius;
        public Circle(double _radius)
        {
            radius = _radius;
        }
        public override string Perimeter()
        {
            return "The perimeter of the circle is " + 2 * Math.PI * radius + "\n";
        }
        public override string Area()
        {
            return "The area of the circle is " + Math.PI * radius * radius + "\n";
        }
    }
    class Triangle : Figure
    {
        public double side1;
        public double side2;
        public double side3;
        public Triangle(double _side1, double _side2, double _side3)
        {
            side1 = _side1;
            side2 = _side2;
            side3 = _side3;
        }
        public override string Perimeter()
        {
            return "The perimeter of the triangle is " + (side1 + side2 + side3) + "\n";
        }
        public override string Area()
        {
            double HalfPer = (side1 + side2 + side3) / 2;
            double HP1 = HalfPer - side1;
            double HP2 = HalfPer - side2;
            double HP3 = HalfPer - side3;
            return "The area of the triangle is " + Math.Sqrt(HalfPer * HP1 * HP2 * HP3) + "\n";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle r = new Rectangle(10, 20);
            Circle c = new Circle(5);
            Triangle t = new Triangle(3, 4, 5);
            Console.WriteLine(r);
            Console.WriteLine(c);
            Console.WriteLine(t);
            Console.ReadKey();
        }
    }
}
