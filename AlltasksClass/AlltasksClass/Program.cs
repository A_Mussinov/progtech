﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlltasksClass
{
    class Car
    {
        public string brand;
        public double volume;
        private string color;
        public Car(string _brand, double _volume, string _color)
        {
            brand = _brand;
            volume = _volume;
            color = _color;
        }
        public string GetColor
        {
            get { return color; }
        }
        public override string ToString()
        {
            return brand +", " + volume + ", " + GetColor;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Car c = new Car("Toyota", 2.7, "black");
            Console.WriteLine(c);
            Console.ReadKey();
        }
    }
}
