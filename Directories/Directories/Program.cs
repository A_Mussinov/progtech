﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Directories
{
    class Program
    {
        static void ShowDirectory(DirectoryInfo Dir, int a)
        {
            //Console.WriteLine(a);
            foreach (FileInfo file in Dir.GetFiles())
            {
                for (int i = 0; i < a; i++)
                {
                    Console.Write("     ");
                }
                Console.WriteLine("File: {0}", file.Name);
            }
            foreach (DirectoryInfo dir in Dir.GetDirectories())
            {
                for (int i = 0; i < a; i++)
                {
                    Console.Write("     ");
                }
                Console.WriteLine("Catalog: {0}", dir.Name);
                ShowDirectory(dir, a+1);
            }
        }
        static void Main(string[] args)
        {
            DirectoryInfo Dir = new DirectoryInfo(@"C:\Аслан\Kniggi");
            ShowDirectory(Dir, 0);
            Console.ReadKey();
        }
    }
}
