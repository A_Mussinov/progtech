﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_1Ex1
{
    class Program
    {
        static void ShowDirectory(DirectoryInfo dir)
        {
            // Show Each File
            foreach (FileInfo file in dir.GetFiles())
            {
                Console.WriteLine("File: {0}", file.FullName);
            }
            // Go through subdirectories
            // recursively
            foreach (DirectoryInfo subDir in dir.GetDirectories())
            {
                ShowDirectory(subDir);
            }
        }
        static void Main(string[] args)
        {
            DirectoryInfo dir = new DirectoryInfo("c:/Аслан/KBTU");
            ShowDirectory(dir);
            Console.ReadKey();
        }
    }
}