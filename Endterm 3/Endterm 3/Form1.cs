﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Endterm_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Point p = new Point (60, 300);
        bool isDown = true;
        Graphics g;
        int ch = 1;

        Point[] pt = new Point[3];

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 3; i++)
            {
                pt[i].X = (i+1)*100;
                pt[i].Y = 350;
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up && isDown == true) isDown = false;
            if (e.KeyCode == Keys.Down && isDown == false) isDown = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            g = this.CreateGraphics();
            g.Clear(Color.White);
            SolidBrush b1 = new SolidBrush(Color.Black);
            SolidBrush b2 = new SolidBrush(Color.Green);

            if (isDown)
            {
                p.Y = 320;
                g.FillEllipse(b1, p.X, p.Y, 20, 20);
            }
            if (isDown == false)
            {
                p.Y = 30;
                g.FillEllipse(b1, p.X, p.Y, 20, 20);
            }

            if (ch == 1)
            {
                g.FillRectangle(b2, pt[0].X -= 65, 350 - 10, 80, 20);
                g.FillRectangle(b2, pt[1].X -= 65, 350 - 10, 80, 20);
                g.FillRectangle(b2, pt[2].X -= 65, 350 - 10, 80, 20);
                ch = 2;
            }

            if (ch == 2)
            {
                g.FillRectangle(b2, pt[0].X - 90, 350 - 10, 80, 20);
                g.FillRectangle(b2, pt[1].X - 90, 350 - 10, 80, 20);
                g.FillRectangle(b2, pt[2].X - 90, 350 - 10, 80, 20);
                ch = 2;
            }

            g.FillRectangle(b2, pt[0].X -= 5, 20 - 10, 80, 20);
            g.FillRectangle(b2, pt[1].X -= 5, 20 - 10, 80, 20);
            g.FillRectangle(b2, pt[2].X -= 5, 20 - 10, 80, 20);

            if (this.Width - pt[2].X == 220)
            { 
                g.FillRectangle(b2, 260, 340, 80, 20);
                pt[0].X = pt[1].X;
                pt[1].X = pt[2].X;
                pt[2].X = 300;
            }
            /*if (isDown) 
            {
                if (p.X - 10 > pt[0].X || p.X + 10 < pt[0].X) MessageBox.Show("Game Over");
            }
            else
            {
                if (p.X - 10 > pt[0].X || p.X + 10 < pt[0].X) MessageBox.Show("Game Over");
            }*/
        }
    }
}
