﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlltasksGraphics
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Brush b1 = new SolidBrush(Color.DodgerBlue);
            Brush b2 = new SolidBrush(Color.Gold);
            e.Graphics.FillRectangle(b1, 0, 0, 160, 100);
            e.Graphics.FillRectangle(b2, 45, 0, 20, 100);
            e.Graphics.FillRectangle(b2, 0, 40, 160, 20);
        }
    }
}
