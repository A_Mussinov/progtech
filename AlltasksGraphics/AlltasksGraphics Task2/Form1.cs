﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlltasksGraphics_Task2
{
    public partial class Form1 : Form
    {
        int z = 311;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Pen p = new Pen(Color.Black, 2);
            e.Graphics.DrawLine(p, z, 150, z, 250);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);

            Pen p = new Pen(Color.Black, 2);
            g.DrawLine(p, z -= 10, 150, z, 250);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);

            Pen p = new Pen(Color.Black, 2);
            g.DrawLine(p, z += 10, 150, z, 250);
        }
    }
}
