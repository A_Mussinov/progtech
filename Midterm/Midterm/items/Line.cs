﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Midterm.items;

namespace Midterm.items
{
    class Line
    {
        public Point Point1;
        public Point Point2;
        public Line(Point a, Point b)
        {
            Point1 = a;
            Point2 = b;
        }
        public static double lineLength(Point a, Point b)
        {
            double length = Math.Sqrt((b.x - a.x)*(b.x-a.x)+(b.y-a.y)*(b.y-a.y));
            return length;
        }
        public override string ToString()
        {
            return "The length of the line is " + lineLength(Point1,Point2).ToString();
        }
    }
}
