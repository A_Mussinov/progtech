﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm.items
{
    [Serializable] public class Point
    {
        public int x, y;
        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;
        }
        public override string ToString()
        {
            return "(" + x.ToString() + "," + y.ToString() + ")";
        }
        public static Point operator +(Point a, Point b)
        {
            Point c = new Point(0, 0);
            c.x = a.x + b.x;
            c.y = a.y + b.y;
            return c;
        }
    }
}
