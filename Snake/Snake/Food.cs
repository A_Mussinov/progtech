﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Snake
{
    class Food
    {
        public Point location;
        public Food()
        {
            location = new Point();
        }
        public void CreateFood()
        {
            Random rnd = new Random();
            location = new Point(rnd.Next(0, 30), rnd.Next(0, 30));
        }
    }
}
