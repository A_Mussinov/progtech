﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake
{
    public partial class Form1 : Form
    {
        Body body;
        Food food;
        Bitmap bm;
        Graphics bGraph;
        Graphics sGraph;
        int level = 1;
        int tick = 10;
        int score = 0;
        Wall[] walls = new Wall[4];

        bool gameover = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            NewLevel();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {            
            if (body.direction == 1) body.GoRight();
            if (body.direction == 2) body.GoDown();
            if (body.direction == 3) body.GoLeft();
            if (body.direction == 4) body.GoUp();

            bGraph.Clear(Color.White);

            bGraph.FillEllipse(new SolidBrush(Color.Red), food.location.X * 10, food.location.Y * 10, 10, 10);

            for (int j = 0; j < 4; j++)
            {
                for (int i = 0; i < 5; i++)
                {
                    bGraph.FillRectangle(new SolidBrush(Color.Black), walls[j].location[i].X * 10, walls[j].location[i].Y * 10, 10, 10);
                }
            }
            for (int i = 0; i < body.length; i++)
            {
                bGraph.FillEllipse(new SolidBrush(Color.Green), body.body[i].X * 10, body.body[i].Y * 10, 10, 10);
                if (body.body[i] == body.body[0] && i > 0) gameover = true;
                for (int k = 0; k < 4; k++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        if (body.body[0] == walls[k].location[j]) gameover = true;
                    }
                }
            }

            sGraph.DrawImage(bm, 0, 0);
            Eat();

            if (body.length == 11 && level == 1)
            {
                level++;
                timer1.Enabled = false;
                MessageBox.Show("You have passed to level " + level);
                NewLevel();
                label4.Text = level.ToString();
            }

            if (body.length == 16 && level == 2)
            {
                level++;
                timer1.Enabled = false;
                MessageBox.Show("You have passed to level " + level);
                NewLevel();
                label4.Text = level.ToString();
            }

            if (body.length == 21 && level == 3)
            {
                timer1.Enabled = false;
                MessageBox.Show("Congratulations! You have won!");
                Application.Exit();
            }

            if (gameover) GameOver();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right && body.direction != 3)
            { 
                body.direction = 1;
            }
            if (e.KeyCode == Keys.Down && body.direction != 4)
            {
                body.direction = 2;
            }
            if (e.KeyCode == Keys.Left && body.direction != 1)
            {
                body.direction = 3;
            }
            if (e.KeyCode == Keys.Up && body.direction != 2)
            {
                body.direction = 4;
            }
        }

        private void NewLevel()
        {
            bm = new Bitmap(300, 300);
            body = new Body();
            body.NewGame();
            for (int i = 0; i < 4; i++)
            {
                walls[i] = new Wall();
            }
            //for (int i = 0; i < 4; i++)
            
            CreateWall();
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.WriteLine(walls[i].location[j].X + " " + walls[i].location[j].Y);
                }
            }

            food = new Food();
            CreateFood();
            bGraph = Graphics.FromImage(bm);
            sGraph = panel1.CreateGraphics();
            timer1.Enabled = true;
            timer1.Interval = 1000 / (tick += 3);
        }

        private void Eat()
        {
            if (body.body[0] == food.location)
            {                
                body.IncreaseLength();
                score++;
                label2.Text = score.ToString();
                CreateFood();
            }
        }

        private void CreateWall()
        {
            bool NoSpace = true;
            do
            {
                NoSpace = false;
                for (int i = 0; i < 4;i++)
                { 
                    walls[i].CreateWall();
                    for (int j = 0; j < 5; j++)
                    {
                        if (walls[i].location[j] == body.body[0])
                        {
                            NoSpace = true;
                            break;
                        }
                    }
                }
            } while (NoSpace);
        }

        private void CreateFood()
        {
            bool NoSpace = true;
            do 
            {
                food.CreateFood();
                NoSpace = false;
                for (int i = 0; i < body.length; i++)
                {
                    if (food.location == body.body[i])
                    {
                        NoSpace = true;
                        break;
                    }
                }
                for(int j = 0; j < 4; j++)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        if (food.location == walls[j].location[i])
                        {
                            NoSpace = true;
                            break;
                        }
                    }
                }
            } while(NoSpace);
        }

        private void GameOver()
        {
            timer1.Enabled = false;
            MessageBox.Show("Game Over");
            Application.Exit();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
