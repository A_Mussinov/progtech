﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Snake
{
    class Wall
    {
        public Point[] location;
        public Wall()
        {
            location = new Point[5];
        }
        public void CreateWall() 
        {
            long time = DateTime.Now.Ticks / TimeSpan.TicksPerSecond;
            Random rnd = new Random((int)time);
            location[0] = new Point(rnd.Next(0, 30), rnd.Next(0, 26));
            int x = location[0].X;
            int y = location[0].Y;
            for (int i = 1; i < 5; i++)
            {
                y++;
                location[i].X = x;
                location[i].Y = y;
            }
        }
    }
}
