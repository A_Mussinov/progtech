﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Levenstein
{
    class Program
    {
        public static int Lev(string a, string b)
        {
            int[,] ar = new int[a.Length + 1, b.Length + 1];

            for (int i = 0; i <= a.Length; i++)
            {
                ar[i, 0] = i;
            }

            for (int j = 0; j <= b.Length; j++)
            {
                ar[0, j] = j;
            }

            for (int i = 1; i <= a.Length; i++)
            {
                for (int j = 1; j <= b.Length; j++)
                {
                    int min1 = (ar[i - 1, j] + 1 < ar[i, j - 1] ? ar[i - 1, j] + 1 : ar[i, j - 1] + 1);
                    int k = (a[i - 1] != b[j - 1] ? 2 : 0);
                    int min2 = (ar[i - 1, j - 1] + k < min1 ? ar[i - 1, j - 1] + k : min1);
                    ar[i, j] = min2;
                }
            }
            return ar[a.Length, b.Length];
        }
        static void Main(string[] args)
        {
            StreamReader all = new StreamReader(@"C:\Аслан\KBTU\2 семестр\Programming Technologies\words.txt");
            StreamReader sr = new StreamReader(@"C:\Аслан\KBTU\2 семестр\Programming Technologies\LevInput.txt");
            StreamWriter sw = new StreamWriter(@"C:\Аслан\KBTU\2 семестр\Programming Technologies\LevOutput.txt");
            /*string s;
            string [] ar = new string [100];
            while ((s = sr.ReadLine()) != null)
            {
                ar = s.Split(' ');
                if 
            }*/

            /*string[] ar = sr.ReadToEnd().Split(' ');
            foreach (string s in ar)
            {
                while (s.IndexOf("  ") != -1)
                {
                    s = s.Replace("  ", " ");
                }
            }*/

            /*string[] ar = new string [r.Length];
            for (int i = 0; i < r.Length; i++)
            {
                if (r[i] != " ")
                {
                    ar[i] = r[i];
                } 
            }*/

            char[] charsToTrim = { ' ' };
            string s = sr.ReadToEnd();
            //while (s.IndexOf("  ") != -1) s = s.Replace("  ", " ");
            //string t = s.Trim();
            //StreamWriter stw = new StreamWriter(@"C:\Аслан\KBTU\2 семестр\Programming Technologies\LevInputNoSpaces.txt");
            //stw.Write(t);
            //stw.Close();
            sr.Close();
            StreamReader str = new StreamReader(@"C:\Аслан\KBTU\2 семестр\Programming Technologies\LevInputNoSpaces.txt");
            string[] ar = str.ReadToEnd().Split(' ');

            List<string> words = new List<string>();
            string line;
            while ((line = all.ReadLine()) != null)
            {
                words.Add(line);
            }
            for (int i = 0; i < ar.Length; i++)
            {
                string s1 = ar[i].ToLower();
                bool correct = false;
                int mindist = -1;
                string tmp = " ";
                for (int j = 0; j < words.Count; j++)
                {
                    string s2 = words[j];
                    int dist = Lev(s1, s2);
                    if (dist == 0) correct = true;
                    else if (mindist == -1 || mindist > dist)
                    {
                        mindist = dist;
                        tmp = s2;
                    }
                }
                if (correct == true)
                {
                    sw.Write(ar[i] + " ");
                    Console.WriteLine(ar[i] + " : ok!");
                }
                else
                {
                    sw.Write(tmp + " ");
                    Console.WriteLine(ar[i] + ": A more proper variant is " + tmp);
                }
            }
            Console.ReadKey();
            all.Close();
            //sr.Close();
            str.Close();
            sw.Close();
        }
    }
}