﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1Part3Ex1
{
    class Person
    {
        public enum Genders : int { Male, Female };
        public string firstName;
        public string lastName;
        public int age;
        public Genders gender;
        public Person(string _firstName, string _lastName, int _age, Genders _gender)
        {
            firstName = _firstName;
            lastName = _lastName;
            age = _age;
            gender = _gender;
        }
        public override string ToString()
        {
            return firstName + " " + lastName + " (" + gender + "), age " + age;
        }


    }
    class Manager : Person
    {
        public string phoneNumber;
        public string officeLocation;
        public Manager(string _firstName, string _lastName, int _age, Genders _gender, string _phoneNumber, string _officeLocation)
            : base(_firstName, _lastName, _age, _gender)
        {
            phoneNumber = _phoneNumber;
            officeLocation = _officeLocation;
        }
        public override string ToString()
        {
            return firstName + " " + lastName + " (" + gender + "), age " + age + ", " + phoneNumber + ", " + officeLocation;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Manager m = new Manager("Tony", "Allen", 32, Person.Genders.Male, "250-13-77", "Almaty, Tole Bi 50");
            Console.WriteLine(m.ToString());
            Console.ReadKey();
        }
    }
}
