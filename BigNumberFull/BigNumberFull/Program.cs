﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumberFull
{
    struct BigNumber
    {
        public int[] a;
        public int sz;
        public string s;

        public BigNumber(string _s)
        {
            s = _s;
            a = new int [1000];
            sz = s.Length;
            for (int i = s.Length - 1, j = 0; i >= 0; i--, j++) 
            {
                a[j] = s[i] -'0';
            }
        }

        public override string ToString()
        {
            string s = "";
            for (int i = sz - 1; i >= 0; i--)
            {
                s = s + a[i].ToString();
            }
            return s;
        }
        public static BigNumber operator +(BigNumber a, BigNumber b)
        {
            BigNumber f = new BigNumber("0");
            int m = Math.Max(a.sz, b.sz);
            int k = 0;
            f.sz = m;
            for (int i = 0; i < m; i++)
            {
                f.a[i] = (a.a[i] + b.a[i] + k);
                k = f.a[i] / 10;
                f.a[i] %= 10;
            }
            if (k > 0)
            {
                f.sz += 1;
                f.a[f.sz - 1] = k;
            }
            return f;
        }
        public static BigNumber operator -(BigNumber a, BigNumber b)
        {
            BigNumber f = new BigNumber("0");
            int m = Math.Max(a.sz, b.sz);
            int k = 0;
            f.sz = m;
            for (int i = 0; i < m; i++)
            {
                f.a[i] = 10 + a.a[i] - b.a[i] - k;
                k = (f.a[i] / 10 + 1) % 2;
                f.a[i] %= 10;
            }

            return f;
        }
        public static BigNumber operator *(BigNumber a, int b)
        {
            BigNumber f = new BigNumber("0");
            int l = 0;
            f.sz = a.sz;
            for (int i = 0; i < a.sz; i++)
            {
                f.a[i] = a.a[i] * b + l;
                l = f.a[i] / 10;
                f.a[i] %= 10;
            }
            if (l > 0)
            {
                f.sz += 1;
                f.a[f.sz - 1] = l;
            }
            int check = 0;
            for (int i = 0; i < f.sz; i++)
            {
                if (f.a[i] == 0) check++;
            }
            if (f.sz == check) f.sz = 1;
            return f;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            BigNumber a = new BigNumber(Console.ReadLine());
            BigNumber b = new BigNumber(Console.ReadLine());
            Console.WriteLine(a-b);
            Console.ReadKey();
        }
    }
}
