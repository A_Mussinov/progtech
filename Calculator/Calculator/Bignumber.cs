﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Bignumber
    {
        public int[] a;
        public int sz;

        public Bignumber(string s)
        {
            a = new int[1000];
            sz = s.Length;
            for (int i = sz - 1, j = 0; i >= 0; i--, j++)
            {
                a[j] = s[i] - '0';
            }
        }

        public override string ToString()
        {
            string s = "";
            for (int i = sz - 1; i >= 0; i--)
            {
                s = s + a[i].ToString();
            }
            return s;
        }

        public static Bignumber operator +(Bignumber a, Bignumber b)
        {
            Bignumber f = new Bignumber("0");
            int m = Math.Max(a.sz, b.sz);
            int k = 0;
            f.sz = m;
            for (int i = 0; i < m; i++)
            {
                f.a[i] = a.a[i] + b.a[i] + k;
                k = f.a[i] / 10;
                f.a[i] %= 10;
            }
            if (k > 0)
            {
                f.sz += 1;
                f.a[f.sz - 1] = k;
            }
            return f;
        }

        public static Bignumber operator -(Bignumber a, Bignumber b)
        {
            Bignumber f = new Bignumber("0");
            int m = Math.Max(a.sz, b.sz);
            int k = 0;
            f.sz = m;
            for (int i = 0; i < m; i++)
            {
                f.a[i] = 10 + a.a[i] - b.a[i] - k;
                k = (f.a[i] / 10 + 1) % 2;
                f.a[i] %= 10;
            }

            return f;
        }
    }
}
