﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Calc
    {
        public Bignumber a, b;
        public Calc()
        {
            a = new Bignumber("0");
            b = new Bignumber("0");
        }

        public Bignumber Count(Bignumber a, Bignumber b, int act)
        {
            Bignumber c = new Bignumber("0");
            Console.WriteLine(a + " " + b);

            if (act == 1) c = a + b;
            if (act == 2) c = a - b;

            if (act == 3)
            {
                double ai = double.Parse(a.ToString());
                double bi = double.Parse(b.ToString());
                double ci = ai * bi;
                c = new Bignumber(ci.ToString());
            }

            if (act == 4)
            {
                double ai = double.Parse(a.ToString());
                double bi = double.Parse(b.ToString());
                double ci = ai / bi;
                c = new Bignumber(ci.ToString());
            }
            Console.WriteLine(c);
            return c;
        }
    }
}
