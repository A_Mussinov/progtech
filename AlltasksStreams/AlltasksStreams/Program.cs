﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace AlltasksStreams
{
    class Program
    {
        static void CompressFile(string inFilename, string outFilename)
        {
            FileStream sourceFile = File.Open(inFilename, FileMode.Open, FileAccess.Read);
            FileStream destFile = new FileStream(outFilename, FileMode.Create);
            GZipStream compStream = new GZipStream(destFile, CompressionMode.Compress);
            int theByte = sourceFile.ReadByte();
            while (theByte != -1)
            {
                compStream.WriteByte((byte)theByte);
                theByte = sourceFile.ReadByte();
            }
            sourceFile.Close();
            compStream.Close();
            destFile.Close();
        }

        static void UncompressFile(string inFilename, string outFilename)
        {
            FileStream sourceFile = File.Open(inFilename, FileMode.Open, FileAccess.Read);
            FileStream destFile = new FileStream(outFilename, FileMode.Create);
            GZipStream compStream = new GZipStream(destFile, CompressionMode.Decompress);
            int theByte = compStream.ReadByte();
            while (theByte != -1)
            {
                destFile.WriteByte((byte)theByte);
                theByte = compStream.ReadByte();
            }
            sourceFile.Close();
            compStream.Close();
            destFile.Close();
        }

        static void Main(string[] args)
        {
            StreamReader sr = new StreamReader(@"C:\Аслан\KBTU\2 семестр\Programming Technologies\ATinput.txt");
            StreamWriter sw = new StreamWriter(@"C:\Аслан\KBTU\2 семестр\Programming Technologies\AToutput.txt");
            string[] a = sr.ReadToEnd().Split(' ');
            sr.Close();
            int n = int.Parse(a[0]);
            int k = 0;
            int[] ar = new int[n];
            for (int i = 0; i < n; i++)
            {
                if (i == n - 1)
                {
                    ar[i] = int.Parse(a[a.Length-1]);
                    k += ar[i];
                    sw.Write(ar[i] + " ");
                    break;
                }
                ar[i] = int.Parse(a[i + 1]);
                k+=ar[i];
                Console.WriteLine(ar[i]);
                sw.Write(ar[i] + " ");
            }
            sw.Write(k);
            sw.Close();
            CompressFile(@"C:\Аслан\KBTU\2 семестр\Programming Technologies\AToutput.txt", @"C:\Аслан\KBTU\2 семестр\Programming Technologies\AToutput.txt.gz");
            UncompressFile(@"C:\Аслан\KBTU\2 семестр\Programming Technologies\AToutput.txt.gz", @"C:\Аслан\KBTU\2 семестр\Programming Technologies\AToutput.txt.txt");
        }
    }
}
