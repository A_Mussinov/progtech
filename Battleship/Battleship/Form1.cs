﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleship
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        SeaBattle sea = new SeaBattle();

        MyButton[,] hor = new MyButton[4, 4];
        MyButton[,] ver = new MyButton[4, 4];
        MyButton[,] all = new MyButton[12, 12];

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    MyButton b = new MyButton(i, j);
                    b.Text = "";
                    b.Location = new Point(25 + j * 25, 25 + i * 25);
                    b.Size = new Size(25, 25);
                    b.BackColor = Color.White;
                    b.Click += btn3_Click;
                    Controls.Add(b);
                    all[i, j] = b;
                }
            }

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    MyButton b = new MyButton(i, j);
                    b.Text = "";
                    b.Location = new Point(25 + j * 25, 35 + 300 + i * 35);
                    b.Size = new Size(25, 25);
                    b.BackColor = Color.White;
                    b.Click += btn1_Click;
                    b.MouseMove += mousemove;
                    Controls.Add(b);
                    hor[i, j] = b;
                }
            }

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    MyButton b = new MyButton(i, j);
                    b.Text = "";
                    b.Location = new Point(35 + 150 + i * 35, 25 + 300 + j * 25);
                    b.Size = new Size(25, 25);
                    b.BackColor = Color.White;
                    b.Click += btn2_Click;
                    Controls.Add(b);
                    ver[i, j] = b;
                }
            }
            ver[0, 0].Visible = false;
        }

        public void seabtn_Click(object sender, EventArgs e)
        {
            
        }

        private void mousemove(object sender, MouseEventArgs e)
        {
            MyButton b = sender as MyButton;
            int x = b.x;
            int y = b.y;
            if (sea.check(x, y, k, true))
            {
                for (int i = y; i<y+k; i++) 
                    
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    hor[i, j].BackColor = Color.White;
                }
            }
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    ver[i, j].BackColor = Color.White;
                }
            }
            MyButton b = sender as MyButton;
            for (int i = 0; i <= b.x; i++)
            {
                hor[b.x, i].BackColor = Color.Yellow;
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    hor[i, j].BackColor = Color.White;
                }
            }
            for (int i = 1; i < 4; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    ver[i, j].BackColor = Color.White;
                }
            }
            MyButton b = sender as MyButton;
            for (int i = 0; i <= b.x; i++)
            {
                ver[b.x, i].BackColor = Color.Yellow;
            }
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            for (int i = b.x - 1; i <= b.x + 1; i++)
            {
                for (int j = b.y - 1; j <= b.y + 1; j++)
                {
                    if (all[i, j].BackColor == Color.Yellow)
                    {
                        MessageBox.Show("Unable to draw here");
                        return;
                    }
                }
            }
            int check1 = 0;
            bool h = false;
            for (int i = 0; i < 4; i++)
            {
                if (hor[i, 0].BackColor == Color.Yellow)
                {
                    check1 = i;
                    h = true;
                    break;
                }
                if (ver[i, 0].BackColor == Color.Yellow)
                {
                    check1 = i;
                    h = false;
                    break;
                }
            }
            if (h)
            {
                if (b.y + check1 > 10) MessageBox.Show("Unable to draw here");
                else
                {
                    for (int i = b.y; i <= b.y + check1; i++)
                    {
                        all[b.x, i].BackColor = Color.Yellow;
                    }
                }
            }
            else
            {
                if (b.x + check1 > 10) MessageBox.Show("Unable to draw here");
                else
                {
                    for (int i = b.x; i <= b.x + check1; i++)
                    {
                        all[i, b.y].BackColor = Color.Yellow;
                    }
                }
            }
        }
    }
}
