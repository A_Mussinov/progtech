﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        MyButton[,] hor = new MyButton[5,5];
        MyButton[,] ver = new MyButton[5,5];
        MyButton[,] all = new MyButton[11, 11];

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    MyButton b = new MyButton(i, j);
                    b.Text = "";
                    b.Location = new Point(j * 25, i * 25);
                    b.Size = new Size(25, 25);
                    b.BackColor = Color.White;
                    b.Click += new EventHandler(btn3_Click);
                    Controls.Add(b);
                    all[i, j] = b;
                }
            }
            for (int i = 1; i <= 4; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    MyButton b = new MyButton(i, j);
                    b.Text = "";
                    b.Location = new Point(j * 25, 300 + i * 35);
                    b.Size = new Size(25, 25);
                    b.BackColor = Color.White;
                    b.Click += new EventHandler(btn1_Click);
                    Controls.Add(b);
                    hor[i, j] = b;
                }
            }
            for (int i = 1; i <= 4; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    MyButton b = new MyButton(i, j);
                    b.Text = "";
                    b.Location = new Point(150 + i * 35, 300 + j * 25);
                    b.Size = new Size(25, 25);
                    b.BackColor = Color.White;
                    b.Click += new EventHandler(btn2_Click);
                    Controls.Add(b);
                    ver[i, j] = b;
                }
            }
            ver[1, 1].Visible = false;
        }
        private void btn1_Click(object sender, EventArgs e)
        {
            for (int i = 1; i <= 4; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    hor[i, j].BackColor = Color.White;
                }
            }
            for (int i = 1; i <= 4; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    ver[i, j].BackColor = Color.White;
                }
            }
            MyButton b = sender as MyButton;
            for (int i = 1; i <= b.x; i++)
            {
                hor[b.x, i].BackColor = Color.Yellow;
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            for (int i = 1; i <= 4; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    hor[i, j].BackColor = Color.White;
                }
            }
            for (int i = 2; i <= 4; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    ver[i, j].BackColor = Color.White;
                }
            }
            MyButton b = sender as MyButton;
            for (int i = 1; i <= b.x; i++)
            {
                ver[b.x, i].BackColor = Color.Yellow;
            }
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            for (int i = b.x - 1; i <= b.x + 1; i++)
            {
                for (int j = b.y - 1; j <= b.y + 1; j++)
                {
                    if (all[i, j].BackColor == Color.Yellow)
                    {
                        MessageBox.Show("Unable to draw here");
                        return;
                    }
                }
            }
            int check1 = 0;
            bool h = false;
            for (int i = 1; i <= 4; i++)
            {
                if (hor[i, 1].BackColor == Color.Yellow)
                {
                    check1 = i;
                    h = true;
                    break;
                }
                if (ver[i, 1].BackColor == Color.Yellow)
                {
                    check1 = i;
                    h = false;
                    break;
                }
            }
            if (h)
            {
                if (b.y + check1 - 1 > 10) MessageBox.Show("Unable to draw here");
                else
                {
                    for (int i = b.y; i < b.y + check1; i++)
                    {
                        all[b.x, i].BackColor = Color.Yellow;
                    }
                }
            }
            else
            {
                if (b.x + check1 - 1 > 10) MessageBox.Show("Unable to draw here");
                else
                {
                    for (int i = b.x; i < b.x + check1; i++)
                    {
                        all[i, b.y].BackColor = Color.Yellow;
                    }
                }
            }
        }
    }
}
