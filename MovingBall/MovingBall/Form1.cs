﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MovingBall
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Ball b = new Ball(700, 100, 20);

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);
            int dx = 10;
            int dy = 10;
            if (b.x + b.r + dx == this.Width || b.x - b.r + dx == 0) dx = -dx;
            if (b.y + b.r + dy == this.Height || b.y - b.r + dy == 0) dy = -dy;
            b.x += dx;
            b.y += dy;
            SolidBrush br = new SolidBrush(Color.Black);
            g.FillEllipse(br, b.x - b.r, b.y - b.r, b.r, b.r);
        }
    }
}
