﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minesweeper
{
    class Saper
    {
        public int n, m, k;
        public int[,] field;
        public Saper(int _n, int _m, int _k)
        {
            n = _n;
            m = _m;
            k = _k;
            field = new int[n + 2, m + 2];
            Field();
        }
        public void Field()
        {
            Random rnd = new Random();
            for (int i = 0; i < k; i++)
            {
                int x = rnd.Next(1, n);
                int y = rnd.Next(1, m);
                field[x, y] = 1;
            }
        }
        public int Move(int x, int y)
        {
            int bomb = 0;
            if (field[x, y] == 1)
            {
                return -1;
            }
            for (int a = x - 1; a <= x + 1; a++)
            {
                for (int b = y - 1; b <= y + 1; b++)
                {
                    bomb += field[a, b];        
                }
            }
            return bomb;
        }
        public void GameOver()
        {
            field = null;
        }
    }
}
