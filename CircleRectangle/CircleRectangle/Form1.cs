﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CircleRectangle
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int x = 390;
        int y = 190;
        int line = 20;
        bool shape = false;
        private void button1_Click(object sender, EventArgs e)
        {
            shape = false;
            timer1.Enabled = true;
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);

            x = 390;
            y = 190;
            line = 20;

            Brush b = new SolidBrush(Color.Green);
            g.FillEllipse(b, x, y, line, line);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            shape = true;
            timer1.Enabled = true;
            Graphics g = this.CreateGraphics();
            g.Clear(Color.White);

            x = 390;
            y = 190;
            line = 20;

            Brush b = new SolidBrush(Color.Green);
            g.FillRectangle(b, x, y, line, line);
        }
            
        private void timer1_Tick(object sender, EventArgs e)
        {
            Color[] color = new Color[] { Color.Blue, Color.Red, Color.Yellow, Color.Green };

            if (shape == false)
            {
                Graphics g = this.CreateGraphics();
                g.Clear(Color.White);
                Brush b = new SolidBrush(color[x % 4]);
                g.FillEllipse(b, x-=1, y-=1, line+=2, line);
            }

            if (shape)
            {
                Graphics g = this.CreateGraphics();
                g.Clear(Color.White);

                Brush b = new SolidBrush(color[x % 4]);
                g.FillRectangle(b, x-=1, y-=1, line+=2, line);
            }
        }
    }
}
