﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Subject.Student
{
    public class Subject
    {
        public string name;
        public int score;

        public Subject(string _name, int _score)
        {
            name = _name;
            score = _score;
        }
    }
    public class Student
    {
        public string name;
        public Subject Subject1;
        public Subject Subject2;
        public Student(string _name, Subject a, Subject b)
        {
            name = _name;
            Subject1 = a;
            Subject2 = b;
        }
        public override string ToString()
        {
            double average = (Subject1.score + Subject2.score) / 2.0;
            return "Average score is " + average.ToString() + ".";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Subject a = new Subject("Programming", 40);
            Subject b = new Subject("English", 30);
            Student s = new Student("Aslan", a, b);
            Console.WriteLine(s);
            Console.ReadKey();
        }
    }
}
